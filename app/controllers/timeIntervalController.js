'use strict';

timeInterval.controller('timeIntervalController', function($scope,$interval, localStorageService) {
    $scope.config = {appTitle:'Time Interval App'};
    $scope.progressTime = {hh: 0, mm:0, ss: 0};
    $scope.events = [];
    $scope.id = 0;
    $scope.activeClass = '';
    $scope.readOnly = false;
    var toggleManualState = function (key) {
        if (key == 1) {
            $scope.manualTaskSaveClass = "visible";
            $scope.manualTaskTrackerClass = "invisible";
        }
        else {
            $scope.manualTaskSaveClass = "";
            $scope.manualTaskTrackerClass = "";
        }
        return key;
    };
    $scope.valueParser = function(string, separator, setProgressTime) {
        if (string != undefined) {
            var fullDate = string.split(separator);
            var userMinutesAdd = 0;
            var userHoursAdd = 0;
            if (fullDate[2] > 60) {
                userMinutesAdd = parseInt(fullDate[2] / 60);
                fullDate[2] = fullDate[2]%60;
            }
            if (fullDate[1] > 60) {
                userHoursAdd = parseInt(fullDate[1]/60);
                fullDate[1] = fullDate[1]%60;
            }
            for (var i = 0; i<3; i++)
                if (isNaN(fullDate[i]))
                    fullDate[i] = 0;
            fullDate[0] = fullDate[0] * 1 + userHoursAdd;
            fullDate[1] = fullDate[1] * 1 + userMinutesAdd;
            fullDate[2] = fullDate[2] * 1;
            if (setProgressTime) {
                $scope.progressTime.hh = fullDate[0] * 1 + userHoursAdd;
                $scope.progressTime.mm = fullDate[1] * 1 + userMinutesAdd;
                $scope.progressTime.ss = fullDate[2] * 1;
            }
            return fullDate;
        }
    };
    var trackerActiveState = function () {
        //$("#start").text('Started!').css({background: '#00A600'});
        $scope.readOnly = true;
        $scope.activeTrackerClass = 'active-timer';
        toggleManualState(0);
    };
    var trackerResetState = function () {
        //$("#start").text('Start').css({background: ''});
        $scope.nowDate = '';
        $scope.readOnly = false;
        $scope.caption = '';
        $scope.activeTrackerClass = '';
    };
    var intervalActiveState = function () {
        timer = $interval(function () {
            $scope.progressTime.ss++;
            if ($scope.progressTime.ss == 60) {
                $scope.progressTime.ss = 0;
                $scope.progressTime.mm++;
            }
            if ($scope.progressTime.mm == 60) {
                $scope.progressTime.mm = 0;
                $scope.progressTime.hh++;
            }
            //$scope.nowDate = $scope.progressTime.hh +"h:"+ $scope.progressTime.mm +"m:"+ $scope.progressTime.ss+"s"
        }, 1000);
        //$scope.nowDate = $scope.progressTime.hh +"h:"+ $scope.progressTime.mm +"m:"+ $scope.progressTime.ss+"s"
    };
    var clearInterval = function (timer) {
        $interval.cancel(timer);
        $scope.progressTime.ss = 0;
        $scope.progressTime.mm = 0;
        $scope.progressTime.hh = 0;
    };
    var getEventTime = function (startTimeArray) {
        var endTime = new Date();
        var endTimeElements = {
            yy: endTime.getFullYear(),
            mth: endTime.getMonth(),
            dd: endTime.getDate(),
            hh: endTime.getHours(),
            mm: endTime.getMinutes(),
            ss: endTime.getSeconds()
        };
        var startTime = startTimeArray;
        var daysInMonth = new Date().daysInMonth();
        var daysInYear = (new Date(startTime.yy,11,31) - new Date(startTime.yy,0,0))/86400000;
        var resultSeconds;
        var resultMinutes;
        var resultHours;
        var resultDays;
        var resultMonths;
        var resultYears;

        if (startTime.ss > endTimeElements.ss) {
            resultSeconds = endTimeElements.ss + 60 - startTime.ss;
            endTimeElements.mm--;
        }
        else {
            resultSeconds = endTimeElements.ss - startTime.ss;
        }
        if (startTime.mm > endTimeElements.mm) {
            resultMinutes = endTimeElements.mm + 60 - startTime.mm;
            endTimeElements.hh--
        }
        else {
            resultMinutes = endTimeElements.mm - startTime.mm;
        }
        if (startTime.hh > endTimeElements.hh) {
            resultHours = endTimeElements.hh + 24 - startTime.hh;
            endTimeElements.dd--
        }
        else {
            resultHours = endTimeElements.hh - startTime.hh;
        }
        if (startTime.dd > endTimeElements.dd) {
            resultDays = endTimeElements.dd + daysInMonth - startTime.dd;
            endTimeElements.mth--;
        }
        else {
            resultDays = endTimeElements.dd - startTime.dd;
        }
        if (startTime.mth > endTimeElements.mth) {
            resultMonths = endTimeElements.mth + daysInYear - startTime.mth;
            endTimeElements.yy--;
        }
        else {
            resultMonths = endTimeElements.mth - startTime.mth;
        }
        resultYears = endTimeElements.yy - startTime.yy;
        var userMinutesAdd = 0;
        var userHoursAdd = 0;
        if (startTime.userSeconds > 60) {
            userMinutesAdd = parseInt(startTime.userSeconds / 60);
            startTime.userSeconds = startTime.userSeconds%60;
        }
        if (startTime.userMinutes > 60) {
            userHoursAdd = parseInt(startTime.userMinutes/60);
            startTime.userMinutes = startTime.userMinutes%60;
        }

        var resultEvent = {
            hh: resultHours + resultDays * 24 + (resultMonths * daysInMonth * 24 )+ (resultYears * daysInYear * 24)+startTime.userHours+userHoursAdd,
            mm: resultMinutes+startTime.userMinutes+userMinutesAdd,
            ss: resultSeconds+startTime.userSeconds,
            caption: startTime.caption
        };
        return resultEvent;
    };

    if (localStorageService.get('startTime')) { //if tracker working
        var continueDate = getEventTime(localStorageService.get('startTime'));
        $scope.progressTime.ss = continueDate.ss;
        $scope.progressTime.mm = continueDate.mm;
        $scope.progressTime.hh = continueDate.hh;
        $scope.caption = continueDate.caption;
        intervalActiveState();
        trackerActiveState();
    }
    var timerActivated = 0;
    var timer;
    if (localStorageService.get('events'))
        $scope.events = localStorageService.get('events');
    $scope.startTimer = function() {
        trackerActiveState();
        if (!timerActivated && !localStorageService.get('startTime')) {
            var initStartTime = new Date();
            var startTimeElements = {
                yy: initStartTime.getFullYear(),
                mth: initStartTime.getMonth(),
                dd: initStartTime.getDate(),
                hh: initStartTime.getHours(),
                mm: initStartTime.getMinutes(),
                ss: initStartTime.getSeconds(),
                userSeconds: $scope.progressTime.ss,
                userMinutes: $scope.progressTime.mm,
                userHours: $scope.progressTime.hh
            };
            intervalActiveState();
            localStorageService.set('startTime', startTimeElements);
            timerActivated = 1;
        }
    };
    $scope.stopTimer = function () {
        if (localStorageService.get('startTime')) {
            clearInterval(timer);
            timerActivated = 0;
            var completedEvent = getEventTime(localStorageService.get('startTime'));
            completedEvent.caption = $scope.caption ? $scope.caption : "No description";
            $scope.events.unshift(completedEvent);
            localStorageService.set('events', $scope.events);
            localStorageService.remove('startTime');
            trackerResetState();

        }
    };
    $scope.removeEvents = function () {
        localStorageService.remove('events');
        $scope.events = [];
    };
    $scope.resumeTask = function (id) {
        var activeTask = $scope.events[id];
        $scope.caption = activeTask.caption;
        $scope.startTimer();
    };
    $scope.updateTask = function (caption, time, id) {
        $scope.events[id].caption = caption;
        var newTime = $scope.valueParser(time, /[a-z]/, false);
        $scope.events[id].hh = newTime[0];
        $scope.events[id].mm = newTime[1];
        $scope.events[id].ss = newTime[2];
        localStorageService.set('events', $scope.events);
    };
    $scope.saveTask = function () {
        if (!localStorageService.get('startTime')) {
            var completedEvent = {ss: 0, mm: 0, hh: 0, caption: ''};
            completedEvent.ss = $scope.progressTime.ss;
            completedEvent.mm = $scope.progressTime.mm;
            completedEvent.hh = $scope.progressTime.hh;
            completedEvent.caption = $scope.caption ? $scope.caption : "No description";
            trackerResetState();
            $scope.events.unshift(completedEvent);
            localStorageService.set('events', $scope.events);
        }
    };
    var toggleClass = 0;
    $scope.manualTask = function() {
        if (!localStorageService.get('startTime'))
            if (toggleClass == 0) {
                toggleClass = toggleManualState(1);
            }
            else {
                toggleClass = toggleManualState(0);
            }
    };
    $scope.deleteEvent = function (id) {
        $scope.events = localStorageService.get('events');
        $scope.events.splice(id,1);
        localStorageService.set('events', $scope.events);
    }
});