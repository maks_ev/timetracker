var timeInterval = angular.module('timeInterval', ['LocalStorageModule']);

timeInterval.config(function (localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('timeInterval');
});