$(document).ready(function(){
    var duration = 500;
    var servicesHolder = $('.services-holder');
    var servicesHolderElem = $(servicesHolder.find('li'));
    var close = $('.close');
    close.click(function(){
        servicesHolder.fadeIn(duration, function() {
            $('.service-unit').fadeOut(duration);
        })
    })
    servicesHolderElem.click(function(){
        var elem = $(this);
        servicesHolder.fadeOut(duration, function() {
            $('.service-unit').eq(elem.index()).fadeIn(duration);
        })
    })
});